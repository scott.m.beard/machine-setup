0. Register Windows

```powershell
slmgr.vbs /ipk <key>
```

1. Install Chocolatey using Powershell admin:
  https://chocolatey.org/install

2. Install the rest:

```powershell
choco install w10privacy --yes
choco install wpd --yes
choco install 7zip.install --yes
choco install keepassxc --yes
choco install FreeDownloadManager --yes
choco install teamviewer --yes
choco install wsl2 --yes
choco install jetbrainstoolbox --yes
choco install rdmfree --yes
choco install vlc --yes
choco install AquaSnap --yes
choco install dataram-ramdisk --yes
choco install keypirinha --yes
choco install okular --yes #pdf and file viewer
```

3. Optional:

```powershell
choco install treesizefree --yes
choco install paint.net --yes
choco install InkScape --yes
choco install handbrake --yes
```

4. Home pc:

```powershell
choco install nvidia-display-driver --package-parameters="'/dch'" --yes
choco install plex --yes
```

5. For development:

```powershell
choco install git.install --yes
choco install gitkraken --yes
choco install vscodium --yes
choco install sql-server-management-studio --yes
choco install docker-desktop --yes
```

6. Old:

```powershell
# choco install nextdns --yes # This isn't needed now
```
